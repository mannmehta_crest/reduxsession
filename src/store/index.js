import { createSlice, configureStore } from '@reduxjs/toolkit';

const numberListInitialState = {
    data: []
}

const numberListSlice = createSlice({
    initialState: numberListInitialState,
    name: 'numberList',
    reducers: {
        addData(state, action) {
            state.data.push(action.payload);
        },
        removeData(state, action) {
            const ind = state.data.indexOf(action.payload);
            state.data.splice(ind, 1);
        }
    }
});

const dummyDataInitialState = {
    data: {},
    loader: false
}

const dummyDataSlice = createSlice({
    initialState: dummyDataInitialState,
    name: 'dummyData',
    reducers: {
        updateData(state, action) {
            state.data = action.payload;
        },
        toggleLoader(state) {
            state.loader = !state.loader;
        }
    }
});

export const getDummyData = () => {
    return (dispatch) => {
        dispatch(dummyDataActions.toggleLoader());
        return fetch('https://jsonplaceholder.typicode.com/todos/1')
            .then(response => response.json())
            .then((json) => {
                dispatch(dummyDataActions.updateData(json));
                dispatch(dummyDataActions.toggleLoader());
            });
    }
}

const store = configureStore({
    reducer: {
        numberListReducer: numberListSlice.reducer,
        dummyDataReducer: dummyDataSlice.reducer
    }
});

export const numberListActions = numberListSlice.actions;
export const dummyDataActions = dummyDataSlice.actions;


export default store;

/** Older Code */

/* 
import { createStore } from 'redux';
import { createStore } from 'redux';

const initialState = {
    data: []
}

export const types = {
    ADD_DATA: "ADD_DATA",
    REMOVE_DATA: "REMOVE_DATA"
}

const reducer = (state = initialState, actions) => {
    if (actions.type === types.ADD_DATA) {
        const tempData = [...state.data];
        tempData.push(actions.payload);
        return {
            data: tempData
        }
    }

    if (actions.type === types.REMOVE_DATA) {
        const tempData = state.data.slice();
        const ind = tempData.indexOf(actions.payload);
        tempData.splice(ind, 1);
        return {
            data: tempData
        }
    }
    return state;
}

const store = createStore(reducer);

export default store; 

*/