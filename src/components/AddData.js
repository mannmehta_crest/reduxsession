import { useState } from "react";
import { useDispatch } from "react-redux";
import { numberListActions } from "../store";
// import { types } from "../store";
function AddData() {
    const [value, setValue] = useState(0);
    const dispatch = useDispatch();
    return <>
        <h1>AddData</h1>
        <input type="number" value={value} onChange={(evt) => { setValue(evt.target.value) }} />
        <button onClick={() => dispatch(numberListActions.addData(value))}>Add Data</button>
        {/* <button onClick={() => dispatch({type: types.ADD_DATA, payload: value})}>Add Data</button> */}
    </>
}

export default AddData;