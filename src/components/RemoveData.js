import { useSelector, useDispatch } from "react-redux";
import { numberListActions } from "../store";
// import { types } from "../store";
function RemoveData() {
    const data = useSelector(state => state.numberListReducer.data);
    // const data = useSelector(state => state.data);
    const dispatch = useDispatch();
    return <>
        <h1>RemoveData</h1>
        <ul>
            {
                data.map((ele, ind) => {
                    return <li key={`RemoveData_${ind}`}>
                        {ele}
                        {" "}
                        <button onClick={() => dispatch(numberListActions.removeData(ele))}>Remove Element</button>
                        {/* <button onClick={() => dispatch({type: types.REMOVE_DATA, payload: ele})}>Remove Element</button> */}
                    </li>
                })
            }
        </ul>
    </>
}

export default RemoveData;