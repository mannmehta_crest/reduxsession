import React, { Component } from 'react';
import { connect } from "react-redux";

class RemoveDataClass extends Component {
    render() {
        return (
            <>
                <h1>RemoveDataClass</h1>
                <ul>
                    {
                        this.props.data.map((ele, ind) => {
                            return <li key={`RemoveDataClass_${ind}`}>
                                {ele}
                                {" "}
                                <button onClick={() => {this.props.removeData(ele)}}>Remove Element</button>
                            </li>
                        })
                    }
                </ul>
            </>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        data: state.data
    }
}

const mapActionsToProps = (dispatch) => {
    return {
        removeData: (ele) => dispatch({type: "REMOVE_DATA", payload: ele})
    }
}

export default connect(mapStateToProps, mapActionsToProps)(RemoveDataClass);