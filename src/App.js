import ShowData from './components/ShowData';
import AddData from './components/AddData';
import { useEffect } from 'react';
import { getDummyData } from './store';
import { useDispatch, useSelector } from 'react-redux';
function App() {
  const dispatch = useDispatch();
  const loader = useSelector((state) => state.dummyDataReducer.loader);
  useEffect(() => {
    dispatch(getDummyData());
  }, [dispatch]);
  return (
    <>
      {loader && "Loading.."}
      {!loader && "Loaded Successfully"}
      <AddData />
      <ShowData />
    </>
  );
}

export default App;
